@extends('layout')

@section('scripts')
    <script src="/js/companies.js"></script>
@endsection

@section('content')
    <div class="col-md-4 p-3">
        <form id="search-form">
            <input type="text" name="term" value="" placeholder="search">
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <div class="col-md-4">
        Companies:
        <div id="companies-list"></div>
    </div>
@endsection
