$(function () {
    const fetchCompanies = (term) => {
        return $.ajax({
            url: `/company/search?q=${term}`
        });
    }

    const render = (companies) => {
        $('#companies-list').html(companies.map(company => {
            return `
            <div class="card text-center">
                ${company.name} <br>
                Reg Number: ${company.regNumber}
            </div>
            `
        }).join('<br>'))
    }

    const validate = (term) => {
        return term.length >= 3 ? Promise.resolve(term): Promise.reject('rr');
    }

    $('#search-form').on('submit', function (e) {
        e.preventDefault();
        const term = $(this).find("[name='term']").val();

        validate(term)
            .then(fetchCompanies)
            .then(render)
            .catch(e => console.log(e))
    })
});

