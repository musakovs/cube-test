<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\Company\CompanyFilter;
use App\Services\Company\CompanyRepository;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function search(
        Request $request,
        CompanyRepository $companyRepository,
        CompanyFilter $companyFilter
    )
    {
        $this->validate($request, [
            'q' => 'string|min:3|required'
        ]);

        return $companyFilter
            ->removeNotActive()
            ->search($request->get('q'))
            ->filter(
                $companyRepository->getAll()
            );
    }

}
