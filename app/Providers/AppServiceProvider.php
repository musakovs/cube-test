<?php

namespace App\Providers;

use App\Services\Company\CompanyProviderInterface;
use App\Services\Company\FakeCompanyProvider;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CompanyProviderInterface::class, FakeCompanyProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
