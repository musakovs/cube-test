<?php

namespace App\Services\Company;

interface CompanyProviderInterface
{
    public function get(): array;
}
