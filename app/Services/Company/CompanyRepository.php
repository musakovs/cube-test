<?php

declare(strict_types=1);

namespace App\Services\Company;

class CompanyRepository
{
    /**
     * @var CompanyMapper
     */
    private $companyMapper;
    /**
     * @var CompanyProviderInterface
     */
    private $companyProvider;

    public function __construct(CompanyProviderInterface $companyProvider, CompanyMapper $companyMapper)
    {
        $this->companyMapper = $companyMapper;
        $this->companyProvider = $companyProvider;
    }

    /**
     * @return Company[]
     */
    public function getAll(): array
    {
        return $this->companyMapper->map(
            $this->companyProvider->get()
        );
    }
}
