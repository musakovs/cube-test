<?php

namespace App\Services\Company;

class FakeCompanyProvider implements CompanyProviderInterface
{
    public function get(): array
    {
        return [
            ['supplierName' => 'Latvenergo', 'registrationNumber' => '324j32AKSLDIHASID', 'isActive' => 'Y'],
            ['supplierName' => 'AENERGY SIA', 'registrationNumber' => '324j32AKSLSDASDAD', 'isActive' => 'Y'],
            ['supplierName' => 'BRIDGE INVESTMENTS SIA', 'registrationNumber' => '324DSADKSLSDASDAD', 'isActive' => 'N'],
            ['supplierName' => 'ENERTY SIA', 'registrationNumber' => '324DSADKSLSDASDAD', 'isActive' => 'Y'],
        ];
    }
}
