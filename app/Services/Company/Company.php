<?php

declare(strict_types=1);

namespace App\Services\Company;

class Company implements \JsonSerializable
{
    private $parameters;

    public function __construct(array $parameters)
    {
        $this->parameters = [
            'name'      => $parameters['name'],
            'active'    => $parameters['active'],
            'regNumber' => $parameters['regNumber']
        ];
    }

    public function name(): string
    {
        return $this->parameters['name'];
    }

    public function isActive(): bool
    {
        return (bool)$this->parameters['active'];
    }

    public function regNumber(): string
    {
        return $this->parameters['regNumber'];
    }

    public function jsonSerialize()
    {
        return $this->parameters;
    }
}
