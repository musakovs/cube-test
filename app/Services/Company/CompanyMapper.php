<?php

namespace App\Services\Company;

class CompanyMapper
{
    /**
     * @param array $companies
     * @return Company[]
     */
    public function map(array $companies): array
    {
        return array_map(function ($company) {
            return new Company([
                'name'      => $company['supplierName'],
                'regNumber' => $company['registrationNumber'],
                'active'    => $company['isActive'] === 'Y'
            ]);
        }, $companies);
    }
}
