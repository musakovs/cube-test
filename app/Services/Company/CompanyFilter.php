<?php

declare(strict_types=1);

namespace App\Services\Company;

class CompanyFilter
{
    /**
     * @var bool
     */
    private $active;

    /**
     * @var string
     */
    private $phrase;

    public function removeNotActive(): self
    {
        $this->active = true;
        return $this;
    }

    public function search(string $phrase): self
    {
        $this->phrase = $phrase;
        return $this;
    }

    /**
     * @param $companies Company[]
     * @return Company[]
     */
    public function filter(array $companies): array
    {
        return array_values(array_filter($companies, [$this, 'filterCompany']));
    }

    private function filterCompany(Company $company): bool
    {
        if ($this->active && !$company->isActive()) {
            return false;
        }

        if (!is_null($this->phrase) && !str_contains($company->name().$company->regNumber(), $this->phrase)) {
            return false;
        }

        return true;
    }
}
