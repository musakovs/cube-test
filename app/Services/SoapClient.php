<?php

namespace App\Services;

use CodeDredd\Soap\Facades\Soap;

class SoapClient
{
    public function fetch()
    {

        $xml = file_get_contents(storage_path('app/xml/companyRequest.xml'));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://test.soa.energo.lv:8011/DMBServices/Private/DMBService?wsdl');
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
        $xml = curl_exec($ch);
        curl_close($ch);

        dd($xml);
    }
}
